$(document).ready(function () {
    'use strict';
    /**
     * variables
     * */
    var $mts_price;
    var ver = iOSversion();
    var $PriceContainer = $('#price-info-container');
    var $Footer = $('.footer');
    var $Page = $('.page');
    $(document).bind('touchmove', false);


    function iOSversion() {
        if ((/iP(hone|od|ad)/.test(navigator.platform)) && (navigator.appVersion.match(/Safari/i))) {
            var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
            return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
        } else {
            return false;
        }
    }
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

    if ((navigator.vendor.indexOf('Apple') > -1) && (!navigator.userAgent.match(/CriOS/i))) {
        if ((ver[0] >= 10) && (ver[1] >= 3 )) {
            $('html, body').offset({top: 2, left: 0});
            if (window.orientation == '90' || window.orientation == '-90') {
                // iPhone 5
                if (window.innerHeight == 320) {
                    // $('html, body').css({'position': 'fixed', 'height': '229px', 'overflow': 'hidden', 'width': '100%'});
                    $('html, body').css({'position': 'relative', 'height': '231px', 'overflow': 'hidden'});
                    $Footer.css('position', 'absolute');
                }
                //iphone6
                if (window.innerHeight == 375) {
                    $('html, body').css({'position': 'fixed', 'height': '100%', 'padding-top': '0px'});
                    $('.header__wrap').css({'position': 'fixed', 'width': '667px', 'paddin-top': '40px'});
                    $Footer.css({'position' : 'fixed', 'padding-bottom' : '44px'});
                }
                // iPhone 6

            }


            // переворот из вертикали в горизонталь
            window.addEventListener('orientationchange', function () {
                if (window.orientation == '90' || window.orientation == '-90') {
                    setInterval(function () {
                        // iPhone 5
                        if (window.innerHeight == 232) {
                            $('.page').css({'padding-top': '0px'});
                            $('#warning').css({'display': 'none'});
                            $('.btn-price-container').css({'margin-top': '-22px'});
                            $('html, body').css({'position': 'relative', 'height': '232px', 'overflow': 'hidden'});
                            $Footer.css('position', 'absolute');
                        }
                        if (window.innerHeight == 460) {
                            $('.page').css({'padding-top': '0px'});
                            $('.btn-price-container').css({'margin-top': '-55px'});
                            $Footer.css({'bottom' : '0px', 'position': 'fixed'});
                        }
                        // iPhone 6
                        if (window.innerHeight == 331) {
                            $('html').height('261px');
                            $('.header__wrap').css({'position': 'fixed', 'width': '667px', 'padding-top': '0px', 'top': '0'});
                            $('.page').css('padding-top', '0px');
                            $Footer.css('padding-bottom','44px');
                        }
                        if (window.innerHeight == 375) {
                            $('html, body').css({'position': 'absolute', 'height': '361px', 'padding-top': '0px'});
                    		$Footer.css({'position' : 'fixed', 'padding-bottom' : '0px'});
                        }

                        return 0;
                    }, 500);
                } else {

                    $('html, body').removeAttr('style');
                    $('.page').removeAttr('style');
                    $('.btn-price-container').removeAttr('style');
                    $Footer.removeAttr('style');
                    $Page.removeAttr('style');
                }

            }, false);
        }
    }

    $('html, body').bind('resize',function () {
        $(window).scrollTop(0);
    });
});
